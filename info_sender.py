import socket
import struct
import sys
import time

from enum import Enum

class Box:
  def __init__(self, left_x = 0, right_x = 0, near_y = 0, far_y = 0, bottom_z = 0, top_z = 0) :
    self.left_x = left_x
    self.right_x = right_x
    self.near_y = near_y
    self.far_y = far_y
    self.bottom_z = bottom_z
    self.top_z = top_z

class Position:
    def __init__(self, x, y, z) :
      self.x = x
      self.y = y
      self.z = z
# def sendHeadInfo() :
class VitalStatus(Enum) :
    NOT_DETECTED = 0
    PRESENCE = 1
    HOLDING_BREATH = 2

class FalldownStatus(Enum) :
    NOT_DETECTED = 0
    STANDING = 1
    FALLDOWN = 2

class InfoSender:
  def __init__(self) :
    self.ip_addr = None
      
    pass
    
  def setDeviceID(self, device_id ) :
    self.device_id = device_id
  
  def setServerIp(self, host_name = '', ip_port = 33333 ) :
    self.host_name = host_name
    self.ip_port = ip_port
    self.ip_addr = None

    # self.Sock.connect((ip_addr, ip_port))
      
  def buildHeader(self) :
    head_data = bytearray(b'')

    frame_head_id = bytearray(b'\x02\x01\x04\x03\x06\x05\x08\x07')
    device_type = bytearray(b'\x02\x00\x00\x00')
    empty_part = bytearray(b'\x00\x00\x00\x00')
    device_id = struct.pack('I', self.device_id)

    head_data += frame_head_id
    head_data += device_type
    head_data += empty_part
    head_data += device_id

    return head_data

  def buildPosition(self, id, position) :
    return struct.pack('Ifff', id, position.x, position.y, position.z)
  
  def buildVitalSignsPacket(self, breath_rate, heart_rate, vital_status) :
    return struct.pack('III', breath_rate, heart_rate, vital_status)

    
  def buildFalldownPacket(self, falldown_status) :
    return struct.pack('I', falldown_status)
    
  def buildRegionPacket(self, range_block) :
    return struct.pack('ffffff', range_block.left_x, range_block.right_x, range_block.near_y, range_block.far_y, range_block.bottom_z, range_block.top_z)

  def sendVitalSigns(self, id, breath_rate, heart_rate, vital_status, position, falldown_status, range_block) :
    if self.ip_addr == None :
      try :
        ip_addr = socket.gethostbyname(self.host_name)
      except socket.gaierror as e:
          print(e)
          print("Could not resolve hostname")
          return False
        

      self.ip_addr = ip_addr
      self.Sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      self.Sock.setblocking(0) 

    print("id: {}, breath rate: {}, heart_rate: {}, vital_status: {}, falldown_staus: {}, positon: {},{},{}".format(id, breath_rate, heart_rate, vital_status, falldown_status, position.x, position.y, position.z))


    frame_data = bytearray(b'')
    frame_data += self.buildHeader()

    frame_data += self.buildPosition(id, position)
    frame_data += self.buildVitalSignsPacket(breath_rate, heart_rate, vital_status)
    frame_data += self.buildFalldownPacket(falldown_status)

    frame_data += self.buildRegionPacket(range_block)

    try :
      # self.Sock.send(bytes(frame_data))
      self.Sock.sendto(bytes(frame_data), (self.ip_addr, self.ip_port))
      # self.Sock.sendto(b"Hello Python!", ("127.0.0.1",8000))
    except Exception as error:
      print("sock send err: ", error)
      return False

    return True
       
  # def sendFallDownInfo(self, id, breath_rate, heart_rate, position, falldown_status, **state) :
  #   frame_data = bytearray(b'')
  #   frame_data.append(self.buildHeader())
  #   frame_data.append(self.buildContent(id, breath_rate, heart_rate, position, falldown_status))

  #   self.Sock.send(bytes(frame_data))
if __name__ == '__main__':
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  while True:
    try:
      sock.sendto(b"Hello Python!", ("127.0.0.1",8000))
    except socket.error as msg:    
    #If no data is received you end up here, but you can ignore
    #the error and continue
      pass  
    time.sleep(1)
     

