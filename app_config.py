import errno
import json
import os
import types
import typing as t
class AppSetting(dict):
  def __init__(self, app_config: dict, defaults: t.Optional[dict] = None) -> None:
    super().__init__(defaults or {})

    self.server_addr = app_config.get('serverAddr', "www.xiaomianao.net.cn")
    self.server_port = app_config.get('serverPort', 33333)
    self.device_id = app_config.get('deviceID', 1)
    self.fall_thresh = app_config.get('fallThresh', -0.4)
    self.distance_thresh = app_config.get('distanceThresh', 0.5)
    self.send_period = app_config.get('sendPeriod', 5)

   

class AppConfig():


  def load_config(config_file_name) :
    if  not os.path.exists(config_file_name) :
      return {}

    with open(config_file_name, 'r') as json_file :
      json_data = json.loads(json_file.read())
      return json_data


  def save_config(config_file_name, app_config) :
    with open(config_file_name, 'w') as json_file :
        json.dump(app_config, json_file, ensure_ascii=False)
      
