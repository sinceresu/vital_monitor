# ----- Imports -------------------------------------------------------
# Standard Imports
import sys
import numpy as np
import time
import math
import os
import statistics
import signal


# Local File Imports
# from gui_threads import *
from gui_parser import uartParser
from gui_common import *
from graphUtilities import *
from  info_sender import InfoSender, Position, Box, VitalStatus, FalldownStatus
from  app_config import AppConfig, AppSetting

currentdir = os.path.dirname(os.path.abspath(__file__))
os.chdir(currentdir)
            
def getConfigDir():
  configDirectory = ''
  try:
      # current_dir = os.path.dirname(sys.argv[0])
      current_dir = os.getcwd()
      configDirectory = os.path.join(current_dir, 'configs')

  except:
      configDirectory = ''
  return configDirectory

frameRate = 20
class VitalMonitor():
    def __init__(self):
        # set window toolbar options, and title

        self.config_file =  os.path.join(getConfigDir(), 'app_config.json')
        self.app_setting = AppSetting(AppConfig.load_config(self.config_file))

        self.info_sender = InfoSender()
        self.info_sender.setDeviceID(self.app_setting.device_id)
        self.info_sender.setServerIp(self.app_setting.server_addr, self.app_setting.server_port)

        # TODO bypass serial read function to also log to a file

        self.frameTime = 50
        self.graphFin = 1
        self.hGraphFin = 1
        self.threeD = 1
        self.lastFramePoints = np.zeros((5,1))
        self.plotTargets = 1
        self.frameNum = 0
        self.lastTID = []

        self.profile = {'startFreq': 60.25, 'numLoops': 64, 'numTx': 3, 'sensorHeight':1.5, 'maxRange':10, 'az_tilt':0, 'elev_tilt':0}
        self.numFrameAvg = 6
        self.stopFlag = False
        self.previousFirstZ = -1
        self.yzFlip = 0
        

        self.fallThresh = self.app_setting.fall_thresh
        self.distanceThresh = self.app_setting.distance_thresh
        self.sendPeriod = self.app_setting.send_period


        self.vitalsPatientData = []
        self.falldown_status = []
        self.falldown_counter = []

        # Persistent point cloud
        self.previousClouds = np.zeros((1,7))

        self.targetList = [] #10 frames of data, 20 tracks, height, relHeight, length, width, age, average height, height delta
        self.targetAge = {} 

        self.numPersistentFrames = 20


    def start_run(self, cliCom, dataCom) :
        self.connectCom(cliCom, dataCom)
        self.selectCfg()
        self.sendCfg()
        self.run()


    def stop(self, signum, frame) :
        self.stopFlag = True
        time.sleep(0.03)
        sys.exit()
 

    def saveSensorPositionCfg(self, height, yaw, pitch):
        fname = self.selectFile()
        with open(fname, 'r') as cfg_file:
            self.cfg = cfg_file.readlines()

        for i, line in enumerate(self.cfg):
            args = line.split()
            if (len(args) > 0):
                # cfarCfg
                if (args[0] == 'sensorPosition'):    
                   self.cfg[i] = "sensorPosition " + str(height) + " " + str(yaw) + " " + str(pitch) + "\n"

        with open(fname, 'w') as cfg_file:
           cfg_file.writelines(self.cfg)


    def setSensorPosition(self, height, yaw, pitch):
        self.profile['sensorHeight'] = height
        self.profile['az_tilt'] = yaw
        self.profile['elev_tilt'] = pitch

        self.saveSensorPositionCfg(height, yaw, pitch)

    
    def saveBoundaryBoxCfg(self, leftX, rightX, nearY, farY, bottomZ, topZ):
        fname = self.selectFile()
        with open(fname, 'r') as cfg_file:
            self.cfg = cfg_file.readlines()

        for i, line in enumerate(self.cfg):
            args = line.split()
            if (len(args) > 0):
                # cfarCfg
                if (args[0] == 'boundaryBox'):    
                   self.cfg[i] = "boundaryBox " + str(leftX) + " " + str(rightX) + " " + str(nearY) + " " + str(farY) + " " + str(bottomZ) + " " + str(topZ) + "\n"

        with open(fname, 'w') as cfg_file:
           cfg_file.writelines(self.cfg)

    def setBoundaryBox(self, leftX, rightX, nearY, farY, bottomZ, topZ):
        self.boundaryBox = Box(leftX, rightX, nearY, farY, bottomZ, topZ)
        self.saveBoundaryBoxCfg(leftX, rightX, nearY, farY, bottomZ, topZ)

    
    def setServerAddress(self, server_addr, server_port):
      self.app_setting.server_addr = server_addr
      self.app_setting.server_port = server_port
      AppConfig.save_config(self.app_setting)


    def updateGraph(self, outputDict):
      pointCloud = None
      numPoints = 0
      tracks = None
      trackIndexs = None
      numTracks = 0
      self.frameNum = 0
      error = 0
      vitalsDict = None
      heights = None


      # Point Cloud
      if ('pointCloud' in outputDict):
        pointCloud = outputDict['pointCloud']

      # Number of Points
      if ('numDetectedPoints' in outputDict):
        numPoints = outputDict['numDetectedPoints']

      # Tracks
      if ('trackData' in outputDict):
        tracks = outputDict['trackData']

      # Heights
      if ('heightData' in outputDict):
          heights = outputDict['heightData']

      # Track index
      if ('trackIndexes' in outputDict):
        trackIndexs = outputDict['trackIndexes']
        
      # Number of Tracks
      if ('numDetectedTracks' in outputDict):
        numTracks = outputDict['numDetectedTracks']

      # Frame number
      if ('frameNum' in outputDict):
        self.frameNum = outputDict['frameNum'] 

      # Error
      if ('error' in outputDict):
        error = outputDict['error']
           
      # Occupancy State Machine
      if ('occupancy' in outputDict):
          occupancyStates = outputDict['occupancy']        

      # Vital Signs Info
      if ('vitals' in outputDict):
        vitalsDict = outputDict['vitals']
        
      if (error != 0):
        print ("Parsing Error on frame: %d" % (self.frameNum))
        print ("\tError Number: %d" % (error))



      # Rotate point cloud and tracks to account for elevation and azimuth tilt
      if (self.profile['elev_tilt'] != 0 or self.profile['az_tilt'] != 0):
        # if (pointCloud is not None):
        #   for i in range(numPoints):
        #     rotX, rotY, rotZ = eulerRot (pointCloud[i,0], pointCloud[i,1], pointCloud[i,2], self.profile['elev_tilt'], self.profile['az_tilt'])
        #     pointCloud[i,0] = rotX
        #     pointCloud[i,1] = rotY
        #     pointCloud[i,2] = rotZ
        if (tracks is not None):
          for i in range(numTracks):
            rotX, rotY, rotZ = eulerRot (tracks[i,1], tracks[i,2], tracks[i,3], self.profile['elev_tilt'], self.profile['az_tilt'])
            tracks[i,1] = rotX
            tracks[i,2] = rotY
            tracks[i,3] = rotZ


        # Shift points to account for sensor height
        if (self.profile['sensorHeight'] != 0):
          # if (pointCloud is not None):
          #     pointCloud[:,2] = pointCloud[:,2] + self.profile['sensorHeight']
          if (tracks is not None):
              tracks[:,3] = tracks[:,3] + self.profile['sensorHeight']



      # Add trackIndexs to the point cloud before adding it to the cumulative cloud
      # if (trackIndexs is not None):
      #   if (self.previousClouds.shape[0] != trackIndexs.shape[0]):
      #     print ("Error in vital_monitor.py: number of points in last frame (" + str(self.previousClouds.shape[0]) + ") does not match number of track indexes (" + str(trackIndexs.shape[0])+ ")")
      #     self.previousClouds = pointCloud
      #     return

      #   else :
      #     self.previousClouds[:, 6] = trackIndexs

      track_position = Position(0, 0, 0)

      targetSize = np.zeros((7,20))

      #target heights        
      if (heights is not None):
        if (len(heights) != len(tracks)):
          print ("WARNING: number of heights does not match number of tracks")
        #populate heights for current tracks
        for height in heights:
          # Find track with correct TID
          tid = -1
          for track in tracks:
            # Found correct track
            if (track[0] == height[0]):
              tid = int(height[0])
              break
          if (tid == -1) :
            break
          self.falldown_status[tid] = FalldownStatus.STANDING
          
          person_height= round(height[1], 2) #absolute height

          if tid in self.lastTID:
            #print("lastTID")
            self.targetAge[tid] = self.targetAge[tid]+1
            age = self.targetAge[tid] #age


            targetSize[3, tid] = self.frameNum
            targetSize[5, tid] = (1/self.numFrameAvg*person_height)+((self.numFrameAvg-1)/self.numFrameAvg)*self.targetList[-1][5,tid] #avg height over numFrameAvg frames


            #need 2 seconds to get accurate height
            if(age>40):
              # for i in range(-1, -10, -1) :
              #   if self.frameNum - self.targetList[i][3, tid] >= 10 :
              #     break
              frame_offset = -15
              delta_height = targetSize[5, tid]-self.targetList[frame_offset][5,tid] #delta height after 10 frames
              # print("frame {}, height {}, old_height {}, delta_height {}.".format(int(self.frameNum), targetSize[5, tid], self.targetList[frame_offset][5,tid], delta_height))

              if (delta_height < self.fallThresh ):
                print("fall down detected, frame {}, height {}, old frame {}, old_height {}, delta_height {}.".format(int(self.frameNum), targetSize[5, tid], 
                                                                              int(self.targetList[frame_offset][3,tid]), self.targetList[frame_offset][5,tid], delta_height))
                self.falldown_status[tid] = FalldownStatus.FALLDOWN

          else:
            self.targetAge[tid] = 1
            targetSize[3, tid] = self.frameNum
            targetSize[5, tid]= person_height

        self.targetList.append(targetSize)
        # If we have more point clouds than we need, delete the oldest ones
        while (len(self.targetList) > self.numPersistentFrames):
            self.targetList.pop(0)

        self.lastTID = tracks      
      else :
        self.lastTID = []


      # self.previousClouds = pointCloud
         
      if (tracks is not None):
        for track in tracks:
          tid = int(track[0])
          if (self.falldown_status[tid] == FalldownStatus.FALLDOWN):
            # self.falldown_status[tid] = FalldownStatus.FALLDOWN
            self.falldown_counter[tid] = 5
          # else :
          #   self.falldown_status[tid] = falldown_status[tid]
             

      if self.frameNum % (self.sendPeriod * frameRate)  == 0: 
        if (tracks is not None):
          for track in tracks:
            tid = int(track[0])
            track_position = Position(track[1], track[2], track[3])
            # print("frame {}".format(int(self.frameNum)))
            falldown_status = FalldownStatus.FALLDOWN if self.falldown_counter[tid] > 0 else self.falldown_status[tid]
            self.info_sender.sendVitalSigns(tid, 15, 60, VitalStatus.PRESENCE.value, track_position, falldown_status.value, 
                                        self.boundaryBox)

            self.falldown_counter[tid]  -= 1




    def connectCom(self, cliCom, dataCom):
      #get parser
      self.parser = uartParser()
      # #init threads and timers
      # self.uart_thread = parseUartThread(self.parser)
      # self.uart_thread.fin.connect(self.updateGraph)
      # self.parseTimer = QTimer()
      # self.parseTimer.setSingleShot(False)
      # self.parseTimer.timeout.connect(self.parseData)        
      try:
        self.parser.connectComPorts(cliCom, dataCom)

#TODO: create the disconnect button action
      except Exception as e:
          print (e)
          print('Unable to Connect')

#TODO: create the disconnect button action
      except Exception as e:
          print (e)
          print('Unable to Connect')

 
    def disconnectCom(self):
        if self.parser:
          self.parser.disconnectComPorts()         
          
#
# Select and parse the configuration file
    def selectCfg(self):
        try:
            self.parseCfg(self.selectFile())
        except Exception as e:
            print(e)
            print('No cfg file selected!')

    

    def selectFile(self):
        configDirectory = getConfigDir()
        return os.path.join(configDirectory ,'AOP_6m_default.cfg')

    

    def parseCfg(self, fname):
        with open(fname, 'r') as cfg_file:
            self.cfg = cfg_file.readlines()
        counter = 0
        chirpCount = 0
        for line in self.cfg:
            args = line.split()
            if (len(args) > 0):
                # cfarCfg
                if (args[0] == 'cfarCfg'):
                    pass
                    #self.cfarConfig = {args[10], args[11], '1'}
                # trackingCfg
                elif (args[0] == 'trackingCfg'):
                    if (len(args) < 5):
                        print ("Error: trackingCfg had fewer arguments than expected")
                        continue
                    self.profile['maxTracks'] = int(args[4])
                    # If we only support 1 patient, hide the other patient window
                    if (self.profile['maxTracks'] == 1):
                        pass
                    # Initialize Vitals output dictionaries for each potential patient
                    for i in range (self.profile['maxTracks']):
                        # Initialize 
                        patientDict = {}
                        patientDict ['id'] = i
                        patientDict ['rangeBin'] = 0
                        patientDict ['breathDeviation'] = []
                        patientDict ['heartRate'] = []
                        patientDict ['breathRate'] = []
                        patientDict ['position'] = []
                        patientDict ['speed'] = []

                        self.vitalsPatientData.append(patientDict)
                        self.falldown_status.append(FalldownStatus.NOT_DETECTED)

                        self.falldown_counter.append(0)


                elif (args[0] == 'AllocationParam'):
                    pass
                    #self.allocConfig = tuple(args[1:6])
                elif (args[0] == 'GatingParam'):
                    pass
                    #self.gatingConfig = tuple(args[1:4])
                elif (args[0] == 'SceneryParam' or args[0] == 'boundaryBox'):
                    if (len(args) < 7):
                        print ("Error: SceneryParam/boundaryBox had fewer arguments than expected")
                        continue
                    self.boundaryLine = counter
                    leftX = float(args[1])
                    rightX = float(args[2])
                    nearY = float(args[3])
                    farY = float(args[4])
                    bottomZ = float(args[5])
                    topZ = float(args[6])
                    self.boundaryBox = Box(leftX, rightX, nearY, farY, bottomZ, topZ)

                elif (args[0] == 'staticBoundaryBox'):
                    self.staticLine = counter
                elif (args[0] == 'profileCfg'):
                    if (len(args) < 12):
                        print ("Error: profileCfg had fewer arguments than expected")
                        continue
                    self.profile['startFreq'] = float(args[2])
                    self.profile['idle'] = float(args[3])
                    self.profile['adcStart'] = float(args[4])
                    self.profile['rampEnd'] = float(args[5])
                    self.profile['slope'] = float(args[8])
                    self.profile['samples'] = float(args[10])
                    self.profile['sampleRate'] = float(args[11])
                    print(self.profile)
                elif (args[0] == 'frameCfg'):
                    if (len(args) < 4):
                        print ("Error: frameCfg had fewer arguments than expected")
                        continue
                    self.profile['numLoops'] = float(args[3])
                    self.profile['numTx'] = float(args[2])+1
                elif (args[0] == 'chirpCfg'):
                    chirpCount += 1
                elif (args[0] == 'sensorPosition'):
                    if (len(args) < 4):
                        print ("Error: sensorPosition had fewer arguments than expected")
                        continue
                    self.profile['sensorHeight'] = float(args[1])
                    print('Sensor Height from cfg = ',str(self.profile['sensorHeight']))
                    self.profile['az_tilt'] = float(args[2])
                    self.profile['elev_tilt'] = float(args[3])
                 # Only used for Small Obstacle Detection
                elif (args[0] == 'occStateMach'):
                    numZones = int(args[1])
                    if (numZones > 2):
                        print('ERROR: More zones specified by cfg than are supported in this GUI')
                
            counter += 1



    def sendCfg(self):
        try:
            self.parser.sendCfg(self.cfg)
            self.configSent = 1
            # self.parseTimer.start(self.frameTime) # need this line 
                
        except Exception as e:
            print(e)
            print ('No cfg file selected!')


    def run(self):
        self.stopFlag = False

        while not self.stopFlag :
            outputDict = self.parser.readAndParseUart()
            if len(outputDict) != 0 :
              self.updateGraph(outputDict)


    def start(self):
      
      signal.signal(signal.SIGINT, self.stop)
      signal.signal(signal.SIGTERM, self.stop)
      self.start_run('/dev/ttyUSB0', '/dev/ttyUSB1')

      vital_monitor.disconnectCom()
    def setCallBack(self, detection_callback):
       self.result_callback = detection_callback

  
if __name__ == "__main__":
    vital_monitor = VitalMonitor()
    
    vital_monitor.start()
